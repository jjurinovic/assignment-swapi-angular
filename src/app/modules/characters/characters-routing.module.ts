import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterItemComponent } from './components/character-item/character-item.component';
import { CharacterListComponent } from './components/character-list/character-list.component';

const routes: Routes = [
  {
    path: '',
    component: CharacterListComponent
  },
  {
    path: 'characters/:id',
    component: CharacterItemComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: []
})
export class CharactersRoutingModule {}
