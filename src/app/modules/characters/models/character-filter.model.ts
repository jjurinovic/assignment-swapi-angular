export interface CharacterFilter {
  movies: string[];
  species: string[];
  from?: number;
  to?: number;
}
