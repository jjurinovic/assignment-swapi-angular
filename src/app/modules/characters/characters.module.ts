import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharacterListComponent } from './components/character-list/character-list.component';
import { CharactersRoutingModule } from './characters-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CharacterFilterComponent } from './components/character-filter/character-filter.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { CharacterItemComponent } from './components/character-item/character-item.component';

@NgModule({
  declarations: [
    CharacterListComponent,
    CharacterFilterComponent,
    CharacterItemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CharactersRoutingModule,
    NgSelectModule,
    FormsModule
  ]
})
export class CharactersModule { }
