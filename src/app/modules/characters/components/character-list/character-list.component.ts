import { Component } from '@angular/core';
import { CharacterService } from 'src/app/core/services/character.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { getCharacterImg, getIdFromUrl } from 'src/app/shared/utils/utils';
import { Character } from '../../models/character.model';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss'],
})
export class CharacterListComponent {
  characters: any = [];
  public minBirthYear: number = 0;
  public maxBirthYear: number = 0;
  public birthYears: number[] = [];

  constructor(
    private _character: CharacterService,
    private _loading: LoadingService
  ) {
    if (this._character.getCharacters().length > 0) {
      this.characters = this._character.getCharacters();
      this.calculateBirthYears();
    } else {
      this.getAllCharacters();
    }
  }

  private getAllCharacters() {
    this._loading.startLoading();
    this._character.getAllCharacters().subscribe((data) => {
      this.characters = data;
      this.calculateBirthYears();
      this._loading.stopLoading();
    });
  }

  private calculateBirthYears() {
    // extract numbers from birth date
    // BBY is negative value and ABY is positive
    const birthYears = this.characters
      .map((character: Character) => character.birth_year)
      .filter((year: string) => year !== 'unknown')
      .map((year: string) =>
        year.includes('BBY') ? parseFloat(year) * -1 : parseFloat(year)
      )
      .sort((a: number, b: number) => a - b);

    this.birthYears = birthYears;
    this.minBirthYear = birthYears[0];
    this.maxBirthYear = birthYears[birthYears.length - 1];
  }

  /**
   * Returns image url of character
   * @param url url of character
   */
  public getCharacterImg(url: string): string {
    return getCharacterImg(url);
  }

  public filterCharacters(data: Character[]): void {
    this.characters = [...data];
  }

  /**
   * Return id from url
   * @param url url for get id
   */
  public getIdFromUrl(url: string) {
    return getIdFromUrl(url);
  }
}
