import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CharacterService } from 'src/app/core/services/character.service';
import { MovieService } from 'src/app/core/services/movie.service';
import { SpecieService } from 'src/app/core/services/specie.service';
import { StarshipService } from 'src/app/core/services/starship.service';
import { Movie } from 'src/app/shared/models/movie.model';
import { Specie } from 'src/app/shared/models/specie.model';
import { Starship } from 'src/app/shared/models/starship.model';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { getCharacterImg, getMovieImg, getSpecieImg, getStarshipImg } from 'src/app/shared/utils/utils';
import { Character } from '../../models/character.model';

@Component({
  selector: 'app-character-item',
  templateUrl: './character-item.component.html',
  styleUrls: ['./character-item.component.scss'],
})
export class CharacterItemComponent implements OnInit {
  private characterId!: number;
  public character?: Character;
  public imgUrl?: string;
  public species: Specie[] = [];
  public movies: Movie[] = [];
  public starships: Starship[] = [];

  constructor(
    private _route: ActivatedRoute,
    private _character: CharacterService,
    private _specie: SpecieService,
    private _movie: MovieService,
    private _loading: LoadingService,
    private _starship: StarshipService
  ) {
  }

  ngOnInit(): void {
    this._route.params.subscribe((params: Params) => {
      this.characterId = params['id'];
      this.getCharacter(this.characterId);
    });
  }

  /** Get character by id */
  private getCharacter(id: number): void {
    this._loading.startLoading();
    this._character.getCharacter(id).subscribe((character: Character) => {
      this.character = character;
      this.imgUrl = getCharacterImg(character.url)
      this.species = this._specie.getSpeciesByUrls(character.species);
      this.movies = this._movie.getMoviesByUrl(character.films);
      this.starships = this._starship.getStarshipsByUrls(character.starships);
      this._loading.stopLoading();
    })
  }

  /** Return image url of movie */
  public getMovieImg(url: string): string {
    return getMovieImg(url)
  }

  /** Return image url of specie */
  public getSpecieImg(url: string): string {
    return getSpecieImg(url)
  }

  /** Return image url of specie */
  public getStarshipImg(url: string): string {
    return getStarshipImg(url)
  }
}
