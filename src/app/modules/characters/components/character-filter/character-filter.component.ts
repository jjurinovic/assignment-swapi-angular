import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IMG_URL } from 'src/app/core/constants/api-constants';
import { CharacterService } from 'src/app/core/services/character.service';
import { MovieService } from 'src/app/core/services/movie.service';
import { SpecieService } from 'src/app/core/services/specie.service';
import { Movie } from 'src/app/shared/models/movie.model';
import { Specie } from 'src/app/shared/models/specie.model';
import { CharacterFilter } from '../../models/character-filter.model';
import { Character } from '../../models/character.model';

@Component({
  selector: 'app-character-filter',
  templateUrl: './character-filter.component.html',
  styleUrls: ['./character-filter.component.scss'],
})
export class CharacterFilterComponent implements OnInit {
  public movies: Movie[] = [];
  public species: Specie[] = [];
  public minYear?: number;
  public maxYear?: number;

  @Input() minBirthYear: number = 0;
  @Input() maxBirthYear: number = 0;
  @Input() birthYears: number[] = [];

  @Output() onFilter: EventEmitter<Character[]> = new EventEmitter();

  public filter: CharacterFilter = {
    movies: [],
    species: [],
    from: undefined,
    to: undefined,
  };

  constructor(
    private _movie: MovieService,
    private _character: CharacterService,
    private _specie: SpecieService
  ) {}

  ngOnInit(): void {
    this.species = this._specie.getSpecies();
    this.movies = this._movie.getMovies();
  }

  public onMovieFilterChange(movies: string[]) {
    this.filter.movies = [...movies].flat(1);
    this.filterCharacters();
  }

  public onSpecieFilterChange(species: string[]) {
    this.filter.species = [...species].flat(1);
    this.filterCharacters();
  }

  public filterFromYear(from: number) {
    this.filter.from = from;
    this.filterCharacters();
  }

  public filterToYear(to: number) {
    this.filter.to = to;
    this.filterCharacters();
  }

  private filterCharacters() {
    let characters = [...this._character.getCharacters()];

    characters = characters.filter((character: Character) => {
      const { birth_year } = character;
      const characterBirthYear = birth_year.includes('BBY')
        ? parseFloat(birth_year) * -1
        : parseFloat(birth_year);

      const fromStatement =
        !this.filter.from ||
        (this.filter.from && this.filter.from! <= characterBirthYear);
      const toStatement =
        !this.filter.to ||
        (this.filter.to && this.filter.to! >= characterBirthYear);
      const moviesStatement =
        this.filter.movies.length === 0 ||
        (this.filter.movies.length > 0 &&
          this.filter.movies.includes(character.url));
      const speciesStatement =
        this.filter.species.length === 0 ||
        (this.filter.species.length > 0 &&
          this.filter.species.includes(character.url));
      const unknownStatement =
        this.filter.from || this.filter.to ? birth_year !== 'unknown' : true;

      return (
        unknownStatement &&
        fromStatement &&
        toStatement &&
        moviesStatement &&
        speciesStatement
      );
    });

    this.onFilter.emit(characters);
  }

  /**
   * Convert year from numerical values to BBY or ABY
   * @param year year like number
   */
  public getYear(year: number) {
    const positiveYear = Math.abs(year);
    return year < 0 ? `${positiveYear} BBY` : `${positiveYear} ABY`;
  }
}
