import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { SpecieService } from "../services/specie.service";

@Injectable({
  providedIn: "root"
})
export class SpecieResolver implements Resolve<any> {
  constructor(private _specie: SpecieService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this._specie.getAllSpecies();
  }
}
