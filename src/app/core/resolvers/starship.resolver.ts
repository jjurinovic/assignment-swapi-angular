import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { StarshipService } from "../services/starship.service";

@Injectable({
  providedIn: "root"
})
export class StarshipResolver implements Resolve<any> {
  constructor(private _starship: StarshipService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this._starship.getAllStarships();
  }
}
