import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { MovieService } from "../services/movie.service";

@Injectable({
  providedIn: "root"
})
export class MovieResolver implements Resolve<any> {
  constructor(private _movie: MovieService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this._movie.getAllMovies();
  }
}
