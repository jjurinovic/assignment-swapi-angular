import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, switchMap, map, Observable } from 'rxjs';
import { Character } from 'src/app/modules/characters/models/character.model';
import { BASE_URL } from '../constants/api-constants';
import { ApiResponse } from '../models/api-response.model';

@Injectable({
  providedIn: 'root',
})
export class CharacterService {
  private pageIndex = 1;
  private firstPageResults: Character[] = [];

  private characters: Character[] = [];

  constructor(private _http: HttpClient) {}

  /**
   * Return characters by given page number
   * @param page Page number
   */
  private getCharactersByPage(
    page: number
  ): Observable<ApiResponse<Character>> {
    const url = `${BASE_URL}people/?page=${page}`;
    return this._http.get<ApiResponse<Character>>(url);
  }

  /**
   * Return all characters for all pages
   */
  public getAllCharacters(): Observable<Character[]> {
    return this.getCharactersByPage(this.pageIndex).pipe(
      switchMap((res: ApiResponse<Character>) => {
        // total pages
        const totalPages = Math.ceil(res.count / 10);

        // save characters from first call
        this.firstPageResults = res.results;

        this.pageIndex = 1;
        const req$ = Array(totalPages - 1)
          .fill(1)
          .map((_, index) => {
            this.pageIndex === totalPages ? this.pageIndex : this.pageIndex++;
            return this.getCharactersByPage(this.pageIndex);
          });
        return forkJoin([...req$]);
      }),
      map((data: any) => {
        // prepare data without pagination properties
        let characters = data.map((character: any) => character.results);
        characters = [...characters, ...this.firstPageResults];
        this.characters = characters.flat(1);
        return characters.flat(1);
      })
    );
  }

  /** Get single character by given id */
  public getCharacter(id: number): Observable<Character> {
    const url = `${BASE_URL}people/${id}`;
    return this._http.get<Character>(url);
  }

  /** Return all characters saved in service variale */
  public getCharacters(): Character[] {
    return this.characters;
  }
}
