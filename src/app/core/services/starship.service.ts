import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, map, Observable, switchMap } from 'rxjs';
import { Starship } from 'src/app/shared/models/starship.model';
import { BASE_URL } from '../constants/api-constants';
import { ApiResponse } from '../models/api-response.model';

@Injectable({
  providedIn: 'root'
})
export class StarshipService {
  private pageIndex = 1;
  private firstPageResults: Starship[] = [];
  private starships: Starship[] = [];

  constructor(private _http: HttpClient) {}

  private getStarshipsByPage(page: number): Observable<ApiResponse<Starship>> {
    const url = `${BASE_URL}starships/?page=${page}`;

    return this._http.get<ApiResponse<Starship>>(url);
  }

  public getAllStarships(): Observable<Starship[]> {
    return this.getStarshipsByPage(this.pageIndex).pipe(
      switchMap((res: ApiResponse<Starship>) => {
        // total pages
        const totalPages = Math.ceil(res.count / 10);

        // save characters from first call
        this.firstPageResults = res.results;

        this.pageIndex = 1;
        const req$ = Array(totalPages - 1)
          .fill(1)
          .map((_, index) => {
            this.pageIndex === totalPages ? this.pageIndex : this.pageIndex++;
            return this.getStarshipsByPage(this.pageIndex);
          });
        return forkJoin([...req$]);
      }),
      map((data: any) => {
        // prepare data without pagination properties
        let starships = data.map((data: any) => data.results);
        starships = [...starships, ...this.firstPageResults];
        this.starships = starships.flat(1);
        return starships.flat(1);
      })
    );
  }

  public getStarships(): Starship[] {
    return this.starships;
  }

  /** Return starships depending on starship url */
  public getStarshipsByUrls(urls: string[]): Starship[] {
    const starships = this.starships.filter((starship: Starship) => {
      return urls.includes(starship.url);
    });

    return starships;
  }
}
