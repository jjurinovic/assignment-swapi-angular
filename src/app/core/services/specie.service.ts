import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, switchMap, map, Observable } from 'rxjs';
import { Specie } from 'src/app/shared/models/specie.model';
import { BASE_URL } from '../constants/api-constants';
import { ApiResponse } from '../models/api-response.model';

@Injectable({
  providedIn: 'root',
})
export class SpecieService {
  private pageIndex = 1;
  private firstPageResults: Specie[] = [];
  private species: Specie[] = [];

  constructor(private _http: HttpClient) {}

  private getSpeciesByPage(page: number): Observable<ApiResponse<Specie>> {
    const url = `${BASE_URL}species/?page=${page}`;

    return this._http.get<ApiResponse<Specie>>(url);
  }

  public getAllSpecies(): Observable<Specie[]> {
    return this.getSpeciesByPage(this.pageIndex).pipe(
      switchMap((res: ApiResponse<Specie>) => {
        // total pages
        const totalPages = Math.ceil(res.count / 10);

        // save characters from first call
        this.firstPageResults = res.results;

        this.pageIndex = 1;
        const req$ = Array(totalPages - 1)
          .fill(1)
          .map((_, index) => {
            this.pageIndex === totalPages ? this.pageIndex : this.pageIndex++;
            return this.getSpeciesByPage(this.pageIndex);
          });
        return forkJoin([...req$]);
      }),
      map((data: any) => {
        // prepare data without pagination properties
        let species = data.map((specie: any) => specie.results);
        species = [...species, ...this.firstPageResults];
        this.species = species.flat(1);
        return species.flat(1);
      })
    );
  }

  public getSpecies(): Specie[] {
    return this.species;
  }

  /** Return species depending on species url */
  public getSpeciesByUrls(urls: string[]): Specie[] {
    const species = this.species.filter((specie: Specie) => {
      return urls.includes(specie.url);
    });

    return species;
  }
}
