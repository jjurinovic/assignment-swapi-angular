import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Movie } from 'src/app/shared/models/movie.model';
import { BASE_URL } from '../constants/api-constants';
import { ApiResponse } from '../models/api-response.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private movies: Movie[] = [];

  constructor(private _http: HttpClient) { }

  /** Get all movies */
  public getAllMovies(): Observable<Movie[]> {
    const url = `${BASE_URL}films/`;

    return this._http.get<ApiResponse<Movie>>(url).pipe(
      map(data => {
        const results = data.results;
        this.movies = results;
        return results;
      })
    )
  }

  /** Return all saved movies */
  public getMovies(): Movie[] {
    return this.movies;
  }

   /** Return movie depending on movie url */
   public getMoviesByUrl(urls: string[]): Movie[] {
    const movies = this.movies.filter((movie: Movie) => {
      return urls.includes(movie.url);
    });

    return movies;
  }
}
