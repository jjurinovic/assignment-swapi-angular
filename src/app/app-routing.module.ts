import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './core/layout/main/main.component';
import { MovieResolver } from './core/resolvers/movie.resolver';
import { SpecieResolver } from './core/resolvers/species.resolver';
import { StarshipResolver } from './core/resolvers/starship.resolver';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    resolve: {
      movies: MovieResolver,
      species: SpecieResolver,
      starships: StarshipResolver
    },
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./modules/characters/characters.module').then(
            (m) => m.CharactersModule
          )
      },

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {})
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
