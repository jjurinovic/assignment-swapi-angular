import { IMG_URL } from "src/app/core/constants/api-constants";

export const getIdFromUrl = (url: string): number => {
  const splittedUrl = url.split('/');
  const urlIndex = splittedUrl.length - 2;
  const id = splittedUrl[urlIndex];
  return parseInt(id);
};


/**
   * Returns image url of character
   * @param url url of character
   */
 export const getCharacterImg = (url: string): string => {
  const id = getIdFromUrl(url);

  return `${IMG_URL}assets/img/characters/${id}.jpg`;
}

/**
   * Returns movie url of movie
   * @param url url of movie
   */
 export const getMovieImg = (url: string): string => {
  const id = getIdFromUrl(url);

  return `${IMG_URL}assets/img/films/${id}.jpg`;
}

/**
   * Returns specie url of specie
   * @param url url of specie
   */
 export const getSpecieImg = (url: string): string => {
  const id = getIdFromUrl(url);

  return `${IMG_URL}assets/img/species/${id}.jpg`;
}

/**
   * Returns starship url of starship
   * @param url url of starship
   */
 export const getStarshipImg = (url: string): string => {
  const id = getIdFromUrl(url);

  return `${IMG_URL}assets/img/starships/${id}.jpg`;
}
